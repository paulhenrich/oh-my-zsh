pd() { cd ~/project/$1;  }
hd() { cd ~/hack/$1;     }
md() { cd ~/mamp/$1;     }

_pd() { _files -W ~/project -/; }
_hd() { _files -W ~/hack -/; }
_md() { _files -W ~/mamp -/; }

compdef _pd pd
compdef _hd hd
compdef _md md
